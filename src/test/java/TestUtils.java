import com.king.test.commons.MiscUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Administrator on 2016/6/11.
 */
public class TestUtils {
	public static long nanoTime(Runnable r) {
		if (r == null) return 0;
		long start = System.nanoTime();
		r.run();
		return System.nanoTime() - start;
	}

	public static void easySleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static String makeParamStr(String... params) {
		if (MiscUtils.isEmpty(params)) return "";
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < params.length - 1; i++) {
			sb.append(params[i]).append('=').append(params[++i]).append('&');
		}
		if (sb.length() > 0)
			sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	public static String httpGet(String urlStr, String... params) {
		if (MiscUtils.isEmpty(urlStr)) return null;

		try {
			URL obj = new URL(urlStr + "?" + makeParamStr(params));
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + urlStr);
			System.out.println("Response Code : " + responseCode);

			try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
				String inputLine;
				StringBuilder response = new StringBuilder();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				return response.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String httpPost(String urlStr, String postData, String... params) {
		if (MiscUtils.isEmpty(urlStr)) return null;

		try {
			URL obj = new URL(urlStr + "?" + makeParamStr(params));
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			con.setDoOutput(true);
			try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
				wr.writeBytes(postData);
				wr.flush();
			}

			System.out.println("\nSending 'POST' request to URL : " + obj);
			System.out.println("Post parameters : " + postData);
			int responseCode = con.getResponseCode();
			System.out.println("Response Code : " + responseCode);

//			try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
//				String inputLine;
//				StringBuilder response = new StringBuilder();
////				while (con.getInputStream().available() > 0 && (inputLine = in.readLine()) != null) {
////					response.append(inputLine);
////				}
//				return response.toString();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
