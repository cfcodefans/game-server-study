import com.king.test.commons.MiscUtils;
import com.king.test.backend.entity.User;
import com.king.test.backend.implementations.Scores;
import com.king.test.backend.implementations.Sessions;
import com.king.test.backend.interfaces.IScores;
import com.king.test.backend.interfaces.ISessions;
import org.junit.Test;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2016/6/11.
 */
public class ScoreTests {
	Scores scores = (Scores) MiscUtils.loadServiceSingleton(IScores.class);
	Sessions sessions = (Sessions) MiscUtils.loadServiceSingleton(ISessions.class);

	@Test
	public void testLoginAndPostScore() {
		final int userId = 1;
		final int levelId = 1;
		final int score = 15;

		User u = sessions.login(userId);
		scores.addScore(levelId, u.getUuid(), score);
		Map<User, Integer> userAndScores = this.scores.getScores(levelId);
		System.out.println(userAndScores);
	}

	@Test
	public void testLoginsAndPostScores() {
		int level1 = 1, level2 = 12, level3 = 123, level4 = 1234;
		User user1 = sessions.login(10), user2 = sessions.login(210), user3 = sessions.login(3210);

		scores.addScore(level1, user1.getUuid(), 321);
		scores.addScore(level1, user2.getUuid(), 123);
		scores.addScore(level1, user3.getUuid(), 213);

		Map<User, Integer> userAndScores = this.scores.getScores(level1);
		userAndScores.entrySet().stream().forEach(kv -> System.out.println(kv));

		System.out.println();
		scores.addScore(level2, user1.getUuid(), 111);
		scores.addScore(level2, user2.getUuid(), 222);
		scores.addScore(level2, user3.getUuid(), 333);

		userAndScores = this.scores.getScores(level2);
		userAndScores.entrySet().stream().forEach(kv -> System.out.println(kv));

		System.out.println();
		for (int i = 1; i < 20; i++) {
			scores.addScore(level3, user1.getUuid(), i);
		}
		userAndScores = this.scores.getScores(level3);
		userAndScores.entrySet().stream().forEach(kv -> System.out.println(kv));

		System.out.println();
		for (int i = 20; i > 0; i--) {
			scores.addScore(level4, user1.getUuid(), i);
		}
		userAndScores = this.scores.getScores(level4);
		userAndScores.entrySet().stream().forEach(kv -> System.out.println(kv));
	}

	@Test
	public void testMultipleScoreInsertion() {
		int level1 = 1, level2 = 12, level3 = 123, level4 = 1234;
		User user1 = sessions.login(10), user2 = sessions.login(210), user3 = sessions.login(3210);

		System.out.println();
		for (int i = 20; i > 0; i--) {
			scores.addScore(level4, user1.getUuid(), i);
			scores.addScore(level4, user2.getUuid(), i);
			scores.addScore(level4, user3.getUuid(), i);
		}
		Map<User, Integer> userAndScores = this.scores.getScores(level4);
		userAndScores.entrySet().stream().forEach(kv -> System.out.println(kv));
	}

	@Test
	public void testConcurrentScoreInsertions() {
		int level1 = 1, level2 = 12, level3 = 123, level4 = 1234;
		ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		for (int i = 0; i < 100; i++) {
			final int _i = i;
			es.submit(() -> {
				User user = sessions.login(_i);
				scores.addScore(level4, user.getUuid(), _i);
			});
		}

		try {
			es.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Map<User, Integer> userAndScores = this.scores.getScores(level4);
		userAndScores.entrySet().stream().forEach(kv -> System.out.println(kv));
	}

	@Test
	public void testConcurrentRepeatScoreInsertions() {
		int level1 = 1, level2 = 12, level3 = 123, level4 = 1234;
		ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		for (int i = 0; i < 100; i++) {
			final int _i = i;
			es.submit(() -> {
				User user = sessions.login(_i);
				scores.addScore(level4, user.getUuid(), _i % 15);
			});
		}

		try {
			es.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Map<User, Integer> userAndScores = this.scores.getScores(level4);
		userAndScores.entrySet().stream().forEach(kv -> System.out.println(kv));
	}

	private String login(int userId) {
		return TestUtils.httpGet(String.format("http://localhost:9989/%d/login", userId));
	}

	private String addScore(int levelId, String sessionKey, int score) {
		return TestUtils.httpPost(String.format("http://localhost:9989/%d/score", levelId), String.valueOf(score), "sessionKey", sessionKey);
	}

	private String getHighScoreList(int levelId) {
		return TestUtils.httpGet(String.format("http://localhost:9989/%d/highscorelist", levelId));
	}

	@Test
	public void integratedTest1() {
		String sessionKey = login(123);
		System.out.println(String.format("sessionKey:\t%s", sessionKey));

		int levelId = 321;
		addScore(levelId, sessionKey, 213);
		System.out.println("added score:\t" + 213);

		System.out.println(String.format("scores:\n%s", getHighScoreList(levelId)));
	}
}
