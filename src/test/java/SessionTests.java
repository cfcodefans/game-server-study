import com.king.test.commons.MiscUtils;
import com.king.test.backend.entity.User;
import com.king.test.backend.implementations.Sessions;
import com.king.test.backend.interfaces.ISessions;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2016/6/11.
 */
public class SessionTests {
	Sessions sessions = (Sessions) MiscUtils.loadService(ISessions.class);


	@Test
	public void testServicLoading() {
		Assert.assertNotNull(sessions);
	}

	@Test
	public void testLogin() {
		int userId = 1;
		User u = sessions.login(1);
		User userById = sessions.getUserById(1);
		User userBySessionKey = sessions.getUserBySessionKey(u.getUuid());

		System.out.println(u);
		System.out.println(userById);
		System.out.println(userBySessionKey);
	}

	@Test
	public void testUuid() {
		Runnable uuidCreation = () -> UUID.randomUUID();
		for (int i = 0; i < 100; i++)
			System.out.println(TestUtils.nanoTime(uuidCreation));
	}

	@Test
	public void testConcurrentLogin() {
		final int userId = 1;

		ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		for (int i = 0; i < 1000; i++) {
			es.submit(() -> sessions.login(userId));
		}
		try {
			es.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println(sessions.getIdAndUsers());
		System.out.println(sessions.getSessionKeyAndUsers());
		sessions.getUsersInOrder().forEach(System.out::println);
	}

	@Test
	public void testConcurrentMultipleLogin() {
		final int[] userIds = {1, 2, 3, 4, 5};

		ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		for (int i = 0; i < 1000; i++) {
			final int t = i;
			es.submit(() -> sessions.login(userIds[t % userIds.length]));
		}
		try {
			es.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Map<Integer, User> idAndUsers = sessions.getIdAndUsers();
		System.out.println(idAndUsers.size());
		System.out.println(idAndUsers);

		Map<String, User> sessionKeyAndUsers = sessions.getSessionKeyAndUsers();
		System.out.println(sessionKeyAndUsers.size());
		System.out.println(sessionKeyAndUsers);

		sessions.getUsersInOrder().forEach(System.out::println);
	}

	@Test
	public void testSessionExpiration() {
		User u = sessions.login(1);
		TestUtils.easySleep(16000);
		System.out.println(System.currentTimeMillis() - u.getLoginTime());
		Assert.assertNull(sessions.getUserById(u.getId()));
	}

	@Test
	public void testSessionUpdate() {
		User u = sessions.login(1);
		TestUtils.easySleep(5000);
		u = sessions.login(1);
		System.out.println(System.currentTimeMillis() - u.getLoginTime());
		TestUtils.easySleep(5000);
		Assert.assertNotNull(sessions.getUserById(u.getId()));
		TestUtils.easySleep(10000);
		Assert.assertNull(sessions.getUserById(u.getId()));
	}
}
