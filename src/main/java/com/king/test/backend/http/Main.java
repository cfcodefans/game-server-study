package com.king.test.backend.http;

import com.king.test.commons.MiscUtils;
import com.king.test.backend.entity.User;
import com.king.test.backend.interfaces.IConfigs;
import com.king.test.backend.interfaces.IScores;
import com.king.test.backend.interfaces.ISessions;
import com.king.test.logging.LogCfgs;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;


@SuppressWarnings("restriction")
public class Main {

    static {
        LogCfgs.load();
    }

    private static final Logger log = Logger.getLogger(Main.class.getSimpleName());

    public static void main(String[] args) throws Exception {
        log.info("Main is starting...");

        startHttpServer();
    }

    private static ExecutorService executors = null;

    private static void startHttpServer() throws Exception {
        IConfigs cfgs = MiscUtils.loadService(IConfigs.class);
        InetSocketAddress addr = new InetSocketAddress(cfgs.getHost(), cfgs.getPort());
        log.info(String.format("http server is launching... %s", addr));
        HttpServer hs = HttpServer.create(addr, 0);

        executors = Executors.newFixedThreadPool(cfgs.getExecutorNumber());

        hs.setExecutor(executors);

        HttpContext mainCtx = hs.createContext("/", Main::dispatch);

        hs.start();
    }

    private static String info(HttpExchange he) {
        return String.format("sessions:\n%s\nscores%s", sessions.toString(), scores.toString());
    }

    private static void dispatch(HttpExchange he) throws IOException {
        Function<HttpExchange, String> handler = getHandler(he);

        OutputStream os = he.getResponseBody();
        String resultStr = "";

        if (handler == null) {
            log.severe("handle is null");
            resultStr = "can't handle:\t" + he.getRequestURI().getPath();
            he.sendResponseHeaders(200, resultStr.length());
            os.write(resultStr.getBytes());
            return;
        }

        resultStr = handler.apply(he);

        byte[] result = resultStr.getBytes();
        if (!MiscUtils.existsInCol(he.getRequestHeaders().get("Accept-encoding"), "gzip")) {
            he.sendResponseHeaders(200, result.length);
            os.write(result);
            return;
        }

        he.getResponseHeaders().set("Content-Encoding", "gzip");
        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
        try (GZIPOutputStream zos = new GZIPOutputStream(os, true)) {
            zos.write(result);
        }
    }

    public static class Router {
        public final String method;

        public final Pattern pattern;
        public final Function<HttpExchange, String> handler;

        public Router(String method, String pathPatternStr, Function<HttpExchange, String> handler) {
            this.method = method;
            this.pattern = Pattern.compile(pathPatternStr);
            this.handler = handler;
        }
    }

    protected static Function<HttpExchange, String> getHandler(HttpExchange he) {
        String pathStr = he.getRequestURI().getPath();
        log.info(pathStr);

        for (Router r : routers) {
            if (r.method.equalsIgnoreCase(he.getRequestMethod())
                    && r.pattern.matcher(pathStr).find()) {
                return r.handler;
            }
        }

        return null;
    }

    private static final Router[] routers = {
        new Router("get", "/info$", Main::info),
        new Router("get", "/[1-9]\\d*/login$", Main::login),
        new Router("post", "/[1-9]\\d*/score$", Main::addScore),
        new Router("get", "/[1-9]\\d*/highscorelist$", Main::getHighScoreList)
    };

    private static String login(HttpExchange he) {
        String pathStr = he.getRequestURI().getPath();

        int userId = MiscUtils.findUnsignedInt(pathStr);
        if (userId < 0) {
            String errMsg = "invalid user id: \t" + userId;
            log.log(Level.SEVERE, errMsg);
            return errMsg;
        }

        User u = sessions.login(userId);

        return u != null ? u.getUuid() : "";
    }

    private static String addScore(HttpExchange he) {
        String reStr = "";
        URI reqUri = he.getRequestURI();
        String pathStr = reqUri.getPath();
        reqUri.getQuery();
        int levelId = MiscUtils.findUnsignedInt(pathStr);
        if (levelId == -1) {
            reStr = "levelid is invalid:\t" + pathStr;
            log.log(Level.WARNING, reStr);
            return reStr;
        }

        List<String> vals = MiscUtils.extractHttpGetReqParams(he).get("sessionKey");
        String sessionKey = MiscUtils.isEmpty(vals) ? null : vals.get(0);

        if (MiscUtils.isBlank(sessionKey)) {
            reStr = "sessionKey is blank";
            log.log(Level.WARNING, reStr);
            return reStr;
        }

        String scoreStr = MiscUtils.readIntoStr(he.getRequestBody());
        int score = MiscUtils.findUnsignedInt(scoreStr);
        if (score < 0) {
            reStr = "score is invalid:\t" + scoreStr;
            log.log(Level.WARNING, reStr);
            return reStr;
        }
        scores.addScore(levelId, sessionKey, score);

        return reStr;
    }

    private static String getHighScoreList(HttpExchange he) {
        String reStr = "";
        URI reqUri = he.getRequestURI();
        String pathStr = reqUri.getPath();
        reqUri.getQuery();
        int levelId = MiscUtils.findUnsignedInt(pathStr);
        if (levelId == -1) {
            reStr = "levelid is invalid:\t" + pathStr;
            log.log(Level.WARNING, reStr);
            return reStr;
        }

        Map<User, Integer> highScores = Main.scores.getScores(levelId);
        StringBuilder sb = new StringBuilder();

        highScores.entrySet().forEach(en -> sb.append(en.getKey().getId()).append("=").append(en.getValue()).append(','));

        if (!MiscUtils.isBlank(sb) && sb.charAt(sb.length() - 1) == ',') {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }

    private static ISessions sessions = MiscUtils.loadServiceSingleton(ISessions.class);
    private static IScores scores = MiscUtils.loadServiceSingleton(IScores.class);
}
