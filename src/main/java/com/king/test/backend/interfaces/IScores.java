package com.king.test.backend.interfaces;

import com.king.test.backend.entity.User;

import java.util.LinkedHashMap;

/**
 * Created by Administrator on 2016/6/9.
 */
public interface IScores {
    void addScore(int levelId, String sessionKey, int score);

    LinkedHashMap<User, Integer> getScores(int levelId);
}
