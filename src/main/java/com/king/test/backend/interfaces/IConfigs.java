package com.king.test.backend.interfaces;

public interface IConfigs {
	
	int DEFAULT_PORT = 9989;
	String DEFAULT_HOST = "0.0.0.0";
	
	String HOST_KEY = "http.host";
	String PORT_KEY = "http.port";

	default String getHost() {
		return DEFAULT_HOST;
	}
	
	default int getPort() {
		return DEFAULT_PORT;
	}
	
	
//	String SESSION_POOL_INIT_SIZE_KEY = "session.pool.init.size";
//	int DEFAULT_SESSION_POOL_INIT_SIZE = 100;
//	default int getSessionPoolInitSize() {
//		return DEFAULT_SESSION_POOL_INIT_SIZE;
//	}
//	
//	String SESSION_POOL_LOAD_FACTOR_KEY = "session.pool.load.factor";
//	int DEFAULT_SESSION_POOL_LOAD_FACTOR = 75;
//	default float getSessionPoolLoadFactor() {
//		return (float) (DEFAULT_SESSION_POOL_LOAD_FACTOR / 100.0);
//	}
	
	
	String EXECUTOR_NUM_KEY = "executor.number";
	int DEFAULT_EXECUTOR_NUM = Runtime.getRuntime().availableProcessors() * 4;
	default int getExecutorNumber() {
		return DEFAULT_EXECUTOR_NUM;
	}
}
