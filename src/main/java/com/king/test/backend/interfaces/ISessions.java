package com.king.test.backend.interfaces;

import com.king.test.backend.entity.User;

/**
 * Created by Administrator on 2016/6/9.
 */
public interface ISessions {
    User login(int userId);
    User getUserBySessionKey(String sessionKey);
    User getUserById(int userId);
}
