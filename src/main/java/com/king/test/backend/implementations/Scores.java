package com.king.test.backend.implementations;

import com.king.test.commons.MiscUtils;
import com.king.test.backend.entity.User;
import com.king.test.backend.interfaces.IConfigs;
import com.king.test.backend.interfaces.IScores;
import com.king.test.backend.interfaces.ISessions;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

/**
 * Created by Administrator on 2016/6/5.
 */
public class Scores implements Serializable, IScores {
	private static final IConfigs cfgs = MiscUtils.loadService(IConfigs.class);

	private static final Logger log = Logger.getLogger(Scores.class.getSimpleName());

	protected final ConcurrentHashMap<Integer, ScorePool> levelAndScores = new ConcurrentHashMap<>();

	protected ISessions sessions = MiscUtils.loadServiceSingleton(ISessions.class);

	public void addScore(int levelId, String sessionKey, int score) {
		if (levelId <= 0 || MiscUtils.isBlank(sessionKey) || score <= 0) {
			return;
		}

		User user = sessions.getUserBySessionKey(sessionKey);
		if (user == null) {
			return;
		}

		levelAndScores.computeIfAbsent(levelId, (_levelId) -> new ScorePool()).addScore(score, user);
	}

	public LinkedHashMap<User, Integer> getScores(int levelId) {
		return levelAndScores.getOrDefault(levelId, new ScorePool()).getScores();
	}

	static class ScorePool {
		private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

		private ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
		private ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

		private Map<User, Integer> userAndScores = new HashMap<>();
		private ConcurrentSkipListMap<Integer, LinkedHashSet<User>> scoreAndUsers = new ConcurrentSkipListMap<>(Comparator.reverseOrder());

		public void addScore(int score, User user) {
			while (!writeLock.tryLock()) {
				if (Thread.interrupted())
					return;
			}

			log.info(String.format("add score: %d for user: %s", score, user));
			try {
				Map.Entry<Integer, LinkedHashSet<User>> lowestScoreAndUsers = scoreAndUsers.lastEntry();
				if (lowestScoreAndUsers == null) {
					userAndScores.put(user, score);
					scoreAndUsers.computeIfAbsent(score, (_score) -> new LinkedHashSet<>()).add(user);
					return;
				}

				Integer lowestScore = lowestScoreAndUsers.getKey();
				if (userAndScores.size() >= 15 && lowestScore >= score) {
					log.info(String.format("score %d is less than lowest score %d", score, lowestScore));
					return;
				}

				Integer previousScore = userAndScores.get(user);
				//as requested, there will be only one score for particular user in result.
				//so, I assumed that only the higher score made by particular user
				// can replace his previous score.
				if (previousScore != null && previousScore >= score) {
					return;
				}

				if (previousScore != null) {
					scoreAndUsers.computeIfPresent(previousScore, (_score, _users) -> {
						_users.remove(user);
						return _users.isEmpty() ? null : _users;
					});
				}

				userAndScores.put(user, score);
				scoreAndUsers.computeIfAbsent(score, (_score) -> new LinkedHashSet<>()).add(user);

				if (userAndScores.size() > 15) {
					final User userWithLowestScore = lowestScoreAndUsers.getValue().iterator().next();
					userAndScores.remove(userWithLowestScore);
					scoreAndUsers.compute(lowestScore, (_score, _users)->{
						_users.remove(userWithLowestScore);
						return _users.isEmpty() ? null : _users;
					});
				}

			} finally {
				writeLock.unlock();
				log.info(String.format("added score: %d for user: %s", score, user));
			}
		}

		public LinkedHashMap<User, Integer> getScores() {
			while (!readLock.tryLock()) {
				if (Thread.interrupted())
					return new LinkedHashMap<>();
			}

			LinkedHashMap<User, Integer> result = new LinkedHashMap<>();
			try {
				scoreAndUsers.forEach((score, users) -> users.forEach(user -> result.put(user, score)));
			} finally {
				readLock.unlock();
			}

			return result;
		}

		@Override
		public String toString() {
			return String.format("\n%s\nuserAndScores\n%s\nscoreAndUsers\n%s",
					super.toString(),
					MiscUtils.stringify(userAndScores.entrySet()),
					MiscUtils.stringify(scoreAndUsers.entrySet()));
		}
	}

	@Override
	public String toString() {
		return String.format("\n%s\nlevelAndScores\n%s\n",
				super.toString(),
				MiscUtils.stringify(levelAndScores.entrySet()));
	}
}
