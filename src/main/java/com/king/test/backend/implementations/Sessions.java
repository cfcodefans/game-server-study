package com.king.test.backend.implementations;

import com.king.test.commons.MiscUtils;
import com.king.test.backend.entity.User;
import com.king.test.backend.interfaces.IConfigs;
import com.king.test.backend.interfaces.ISessions;

import java.io.Serializable;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class Sessions implements Serializable, ISessions {
	private static final long serialVersionUID = 7673065625942797382L;

	private static final Logger log = Logger.getLogger(Sessions.class.getSimpleName());
	public static final int TEN_MINS = 1000 * 60 * 10;

	public static final UUID uuid() {
		return UUID.randomUUID();
	}

	private static final IConfigs cfgs = MiscUtils.loadService(IConfigs.class);

	private ConcurrentMap<Integer, User> userPool = new ConcurrentHashMap<>();
	private ConcurrentMap<String, User> sessionPool = new ConcurrentHashMap<>();


	private ScheduledExecutorService timedCleaner = Executors.newScheduledThreadPool(1);

	private ConcurrentLinkedDeque<MiscUtils.Pair<Long, User>> usersInOrder = new ConcurrentLinkedDeque<>();

	//Not scalable!!! who knows how long this timed cleanup will take for high number of users.
	public Sessions() {
		timedCleaner.scheduleAtFixedRate(this::clean, 0, 5, TimeUnit.SECONDS);
	}

	protected long getTimeThreshold() {
		return System.currentTimeMillis() - TEN_MINS;
	}

	protected void clean() {
		log.info("clean expired sessions:\t" + usersInOrder.size());
		long tenMinsAgo = getTimeThreshold();
		for (MiscUtils.Pair<Long, User> p = usersInOrder.peekFirst(); p != null; p = usersInOrder.peekFirst()) {
			if (p.v1 > tenMinsAgo) {
				break;
			}

			MiscUtils.Pair<Long, User> _p = usersInOrder.pollFirst();
			User user = _p.v2;

			if (null == userPool.compute(user.getId(), (Integer uid, User u) -> u.validateTimeout(tenMinsAgo))) {
				if (null == sessionPool.compute(user.getUuid(), (String uuid, User u) -> u.validateTimeout(tenMinsAgo))) {
					logoff(user);
				}
			}
		}
	}

	protected void logoff(User user) {
		//update scores?
		log.info(String.format("logoff %s", user));
	}

	public User login(int userId) {
		if (userId < 0) {
			throw new IllegalArgumentException(String.format("user id: %d should not be negative", userId));
		}
		//be noted, atomic operation is implemented by synchronized keyword inside ConcurrentHashMap
		User user = userPool.compute(userId, this::updateSession);
		sessionPool.put(user.getUuid(), user);
		return user;
	}

	protected User updateSession(int userId, User _u) {
		if (_u == null) {
			_u = new User(userId);
			_u.setUuid(uuid().toString());
		} else {
			_u.update();
		}

		final User u = _u;
		//Not scalable!!! what if the backend is running on cluster among several servers?
		//redis could help as central storage, then risks single point failure
		usersInOrder.push(new MiscUtils.Pair<>(u.getLoginTime(), u));

		return _u;
	}

	public User getUserById(int userId) {
		if (userId < 0) {
			throw new IllegalArgumentException(String.format("user id: %d should not be negative", userId));
		}
		return userPool.get(userId);
	}

	public User getUserBySessionKey(String sessionKey) {
		if (MiscUtils.isBlank(sessionKey)) return null;
		return sessionPool.get(sessionKey);
	}

	public Map<Integer, User> getIdAndUsers() {
		return userPool;
	}

	public Map<String, User> getSessionKeyAndUsers() {
		return sessionPool;
	}

	public Queue<MiscUtils.Pair<Long, User>> getUsersInOrder() {
		return usersInOrder;
	}

	@Override
	public String toString() {
		return String.format("\n%s\nsessionPool\n%s\n",
				super.toString(),
				MiscUtils.stringify(sessionPool.entrySet()));
	}
}
