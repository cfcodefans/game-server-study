package com.king.test.backend.implementations;

import com.king.test.backend.interfaces.IConfigs;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configs extends Properties implements IConfigs {

	private static final long serialVersionUID = -5406828637009301647L;
	private static final Logger log = Logger.getLogger(Configs.class.getName());

	protected void loadCfgs() {
		try {
			try (final InputStream in = Configs.class.getResourceAsStream(System.getProperty("com.king.test.cfg.properties", "/cfgs.properties"))) {
				load(in);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "failed to load configuations", e);
		}
	}

	public Configs() {
		loadCfgs();
	}

	public String getHost() {
		return this.getProperty(HOST_KEY, DEFAULT_HOST);
	}

	public int getPort() {
		int port = getInt(PORT_KEY, DEFAULT_PORT);
		if (port < 1024 || port > 65535) {
			log.log(Level.WARNING, String.format("port is misconfigured: %d, using default %d", port, DEFAULT_PORT));
			return DEFAULT_PORT;
		}
		return port;
	}
	
	public int getInt(String key, long defaultValue) {
		return Long.valueOf(getLong(key, defaultValue)).intValue();
	}

	public long getLong(String key, long defaultValue) {
		if (key == null)
			throw new IllegalArgumentException("key is null!");

		String portStr = "";
		try {
			portStr = this.getProperty(key, String.valueOf(defaultValue));
			long port = Long.parseUnsignedLong(portStr);
			return port;
		} catch (NumberFormatException e) {
			log.log(Level.WARNING, String.format("key: %s is misconfigured: %s, using default %d", key, portStr, defaultValue), e);
		}

		return defaultValue;
	}
	
//	public int getSessionPoolInitSize() {
//		int size = getInt(SESSION_POOL_INIT_SIZE_KEY, DEFAULT_SESSION_POOL_INIT_SIZE);
//		if (size <= 0) {
//			log.log(Level.WARNING, String.format("%s is misconfigured: %d, should be greater than 0, using default %d",
//					SESSION_POOL_INIT_SIZE_KEY,
//					size,
//					DEFAULT_SESSION_POOL_INIT_SIZE));
//			return DEFAULT_SESSION_POOL_INIT_SIZE;
//		}
//		return size;
//	}
//
//	public float getSessionPoolLoadFactor() {
//		int factor = getInt(SESSION_POOL_LOAD_FACTOR_KEY, DEFAULT_SESSION_POOL_LOAD_FACTOR);
//		if (factor <= 0 || factor >= 100) {
//			log.log(Level.WARNING, String.format("%s is misconfigured: %d, should be between 1 and 99, using default %d",
//					SESSION_POOL_LOAD_FACTOR_KEY,
//					factor,
//					DEFAULT_SESSION_POOL_LOAD_FACTOR));
//			factor = DEFAULT_SESSION_POOL_LOAD_FACTOR;
//		}
//		return (float) (factor / 100.0);
//	}
	
	public int getExecutorNumber() {
		int executorNumber = getInt(EXECUTOR_NUM_KEY, DEFAULT_EXECUTOR_NUM);
		if (executorNumber <= 0) {
			log.log(Level.WARNING, String.format("%s is misconfigured: %d, should be greater than 0, using default %d",
					EXECUTOR_NUM_KEY,
					executorNumber,
					DEFAULT_EXECUTOR_NUM));
			return DEFAULT_EXECUTOR_NUM;
		}
		return executorNumber;
	}

}
