package com.king.test.backend.entity;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

	private static final long serialVersionUID = -4307768555252581390L;

	public User() {
		loginTime = System.currentTimeMillis();
	}
	
	public User(int _id) {
		this();
		this.id = _id;
	}

	public synchronized User validateTimeout(long time) {
		return time > loginTime ? null : this;
	}
	
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		if (id < 0) {
			throw new IllegalArgumentException("user id should be greater than 0!");
		}
		this.id = id;
	}

	private String uuid;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	private volatile long loginTime;

	public Long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", uuid=" + uuid + ", loginTime=" + loginTime + "]";
	}

	public synchronized User update() {
		this.loginTime = System.currentTimeMillis();
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		User user = (User) o;
		return Objects.equals(id, user.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public static int compareById(User u1, User u2) {
		if (u1 == null && u2 == null) return 0;
		if (u1 == null && u2 != null) return -1;
		if (u1 != null && u2 == null) return 1;
		return Integer.compare(u1.id, u2.id);
	}
}
