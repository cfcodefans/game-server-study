package com.king.test.commons;

import com.sun.net.httpserver.HttpExchange;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class MiscUtils {
	public static boolean isBlank(CharSequence str) {
		if (isEmpty(str)) return true;
		return str.chars().anyMatch(c -> Character.isSpaceChar(c) || Character.isWhitespace(c));
	}

	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}

	public static boolean isEmpty(Map map) {
		return map == null || map.isEmpty();
	}

	public static <T> boolean isEmpty(T[] array) {
		return array == null || array.length == 0;
	}

	public static Map<Object, Object> map(Object... kvs) {
		HashMap<Object, Object> map = new HashMap<Object, Object>();
		if (isEmpty(kvs)) {
			return map;
		}

		for (int i = 0, j = kvs.length - kvs.length % 2; i < j; ) {
			map.put(kvs[i++], kvs[i++]);
		}

		if (map.size() * 2 < kvs.length) {
			map.put(kvs[kvs.length - 1], null);
		}

		return map;
	}

	public static boolean isEmpty(Collection col) {
		return col == null || col.isEmpty();
	}

	public static boolean contains(Collection col, Object el) {
		return isEmpty(col) ? false : col.contains(el);
	}

	public static boolean existsInCol(Collection<String> strCol, String str) {
		return isEmpty(strCol) ? false : strCol.stream().filter(el -> el != null).anyMatch(el -> el.contains(str));
	}

	private final static String MAX_INT = String.valueOf(Integer.MIN_VALUE);

	public static class Pair<T1, T2> {
		public T1 v1;
		public T2 v2;

		public Pair(T1 v1, T2 v2) {
			this.v1 = v1;
			this.v2 = v2;
		}

		@Override
		public String toString() {
			return "Pair{" +
					"v1=" + v1 +
					", v2=" + v2 +
					'}';
		}
	}

	private static Pattern unsignedIntPattern = Pattern.compile("[1-9]\\d*");

	public static int findUnsignedInt(String str) {
		return findUnsignedInt(str, 0);
	}

	public static int findUnsignedInt(String str, int start) {
		if (isBlank(str)) return -1;

		Matcher matcher = unsignedIntPattern.matcher(str);
		if (matcher.matches()) {
			return Integer.parseInt(matcher.group());
		}

		if (matcher.find(start)) {
			return Integer.parseInt(matcher.toMatchResult().group());
		}

		return -1;
	}

	public static <I> I loadService(Class<I> interfaceClass) {
		if (interfaceClass == null) return null;
		I serviceInstance = ServiceLoader.load(interfaceClass).iterator().next();
		return serviceInstance;
	}

	private static Map<Class, Object> serviceClassAndInstances = Collections.synchronizedMap(new HashMap<>());

	public static <I> I loadServiceSingleton(Class<I> interfaceClass) {
		if (interfaceClass == null) return null;
		return interfaceClass.cast(serviceClassAndInstances.computeIfAbsent(interfaceClass, (_interfaceClass) -> loadService(_interfaceClass)));
	}

	public static Map<String, List<String>> extractHttpGetReqParams(HttpExchange he) {
		Map<String, List<String>> params = new HashMap<>();
		if (he == null) {
			return params;
		}
		return getStringListMap(he.getRequestURI().getQuery());
	}

	public static Map<String, List<String>> extractHttpFormReqParams(HttpExchange he) {
		Map<String, List<String>> params = new HashMap<>();
		if (he == null) {
			return params;
		}

		return getStringListMap(readIntoStr(he.getRequestBody()));
	}

	private static Map<String, List<String>> getStringListMap(String reqStr) {
		Map<String, List<String>> params = new HashMap<>();
		if (isBlank(reqStr)) {
			return params;
		}

		String[] fragments = reqStr.split("&");
		Stream.of(fragments)
				.filter(f -> !MiscUtils.isBlank(f))
				.map(MiscUtils::parseParamPair)
				.filter(kvPair -> kvPair != null)
				.forEach(kvPair -> params.computeIfAbsent(kvPair.v1, k -> new ArrayList<>()).add(kvPair.v2));

		return params;
	}

	public static Pair<String, String> parseParamPair(String fragment) {
		if (isBlank(fragment)) return null;

		String[] keyAndVal = fragment.split("=");
		String key = keyAndVal[0];
		String val = (keyAndVal.length <= 1) ? null : keyAndVal[1];
		return new Pair<>(key, val);
	}

	public static String readIntoStr(InputStream is) {
		if (is == null) return null;
		StringBuilder sb = new StringBuilder();
		DataInputStream dis = new DataInputStream(is);

		try {
			for (int c = is.read(); c != -1; c = is.read()) {
				sb.append((char) c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	public static String stringify(Collection col) {
		if (isEmpty(col)) return "{}";

		StringBuilder sb = new StringBuilder("\n");

		col.stream().map(String::valueOf).forEach(line->sb.append(line).append('\n'));

		return sb.toString();
	}
}
