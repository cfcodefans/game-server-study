package com.king.test.logging;

import java.io.InputStream;
import java.util.logging.LogManager;

public class LogCfgs {
	public LogCfgs() {
		try (final InputStream in = LogCfgs.class.getResourceAsStream("/logging.properties")) {
			LogManager.getLogManager().readConfiguration(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void load() {
		// load log configurations in jar by default
		// for logging configuration alternatives, we can set jvm argument named
		// "java.util.logging.config.file", whose settings would overwrite the
		// default
		System.setProperty("java.util.logging.config.class", LogCfgs.class.getName());
	}
}
